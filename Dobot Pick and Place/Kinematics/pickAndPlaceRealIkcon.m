function  pickAndPlaceRealIkcon()

clc 
close all


z_ofset_moving_cube     =   [ 0,    0,  0.035] ;
z_ofset_suction         =   [ 0,    0, -0.048] ;

z_ofset_dropping_L1     =   [ 0,    0,  -0.035 ];
z_ofset_dropping_L2     =   [ 0,    0,  -0.016 ];
z_ofset_moving_cube_L3  =   [ 0,    0,  0.045 ];
z_ofset_dropping_cube_L3 =  [ 0,    0,  0.007 ];

redCube1 =   [0.2 0.2 0];
redCube2 =   [0 0.3 0];
greenCube1 = [0.1 0.2 0];
greenCube2 = [0.15 0.25 0];
blueCube1 =  [0 0.2 0];
blueCube2 =  [0.1 0.25 0];

center_redCube1     = redCube1      + [0.01,    0.022,  0] 
center_redCube2     = redCube2      + [0.005,   0.022,  0]
center_greenCube1   = greenCube1    + [0.00,    0.016,  0];
center_greenCube2   = greenCube2    + [0.008,   0.02,   0];
center_blueCube1    = blueCube1     + [-0.002,  0.015,  0];
center_blueCube2    = blueCube2     + [0.011,   0.024,  0];

center_redCube1_moving      =   center_redCube1 +	 z_ofset_moving_cube ;
center_redCube1_dropping    =   center_redCube1 +	 z_ofset_suction ;
center_redCube2_moving      =   center_redCube2 +	 z_ofset_moving_cube ;
center_redCube2_dropping    =	center_redCube2 +	 z_ofset_suction ;
center_greenCube1_moving    =	center_greenCube1 +	 z_ofset_moving_cube ;
center_greenCube1_dropping  =	center_greenCube1 +	 z_ofset_suction ;
center_greenCube2_moving    =	center_greenCube2 +	 z_ofset_moving_cube ;
center_greenCube2_dropping  =	center_greenCube2 +	 z_ofset_suction ;
center_blueCube1_moving     =   center_blueCube1  +  z_ofset_moving_cube ;
center_blueCube1_dropping   =   center_blueCube1  +  z_ofset_suction ;
center_blueCube2_moving     =   center_blueCube2  +  z_ofset_moving_cube ;
center_blueCube2_dropping   =   center_blueCube2 +   z_ofset_suction ;

% Level 1
final_redCube1_moving   =   [0.288  0   0]  +   z_ofset_moving_cube;
final_redCube1_dropping =   [0.288  0   0]  +   z_ofset_dropping_L1;
final_redCube2_moving   =   [0.252  0   0]  +   z_ofset_moving_cube;   
final_redCube2_dropping =   [0.252  0   0]  +   z_ofset_dropping_L1;    
final_greenCube1_moving =   [0.220  0   0]  +   z_ofset_moving_cube;    
final_greenCube1_dropping = [0.220  0   0]  +   z_ofset_dropping_L1;   

% Level 2
final_greenCube2_moving     = [0.245    0       0       ]  + z_ofset_moving_cube;
final_greenCube2_dropping   = [0.245    0       0       ]  + z_ofset_dropping_L2 ;
final_blueCube1_moving      = [0.280    0       0       ]  + z_ofset_moving_cube;
final_blueCube1_dropping    = [0.280    0       0       ]  + z_ofset_dropping_L2;

% Level 3
final_blueCube2_moving      = [0.265    0  0  ]  + z_ofset_moving_cube_L3;
final_blueCube2_dropping    = [0.265    0  0  ]  + z_ofset_dropping_cube_L3;

%rosinit('http://10.42.0.1:11311')

suctioncupsvc_ = rossvcclient('/dobot_magician/end_effector/set_suction_cup');
suctioncupmsg_ = rosmessage(suctioncupsvc_);

cartsvc_ = rossvcclient('/dobot_magician/PTP/set_cartesian_pos'); cartmsg_ = rosmessage(cartsvc_);
jangsvc_ = rossvcclient('/dobot_magician/PTP/set_joint_angles'); jangmsg_ = rosmessage(jangsvc_);

suctioncupmsg_.IsEndEffectorEnabled=1;
suctioncupmsg_.EndEffectorState=1;
suctioncupsvc_.call(suctioncupmsg_);

% Red 1
    dobotIkconReal(center_redCube1_moving, jangsvc_, 0);                            % Go To Cube 
    suctioncupmsg_.EndEffectorState=1; suctioncupsvc_.call(suctioncupmsg_);
    dobotIkconReal(center_redCube1_dropping, jangsvc_, 0);                          % Pick Cube
    pause(1);                                                                       % Activate suction
    dobotIkconReal(center_redCube1_moving, jangsvc_, 0);                            % Go up in the z axis, ( to don't hit any other cube )

    q = dobotIkconReal(final_redCube1_moving, jangsvc_, 0);                         % Going to final position &  Adjust Rotation of the cube

    q = dobotIkconReal(final_redCube1_dropping, jangsvc_, -2.6);                          %  Reaching final position & Adjust Rotation of the cube

    pause(1);   suctioncupmsg_.EndEffectorState=0; suctioncupsvc_.call(suctioncupmsg_);  pause(1); % Desactivate suction
    dobotIkconReal(final_redCube1_moving, jangsvc_,0);                              % Go up in the z axis, ( to don't hit any other cube )


% Red 2
    dobotIkconReal(center_redCube2_moving, jangsvc_, 0 );                           % Go To Cube
    suctioncupmsg_.EndEffectorState=1; suctioncupsvc_.call(suctioncupmsg_);         % Activate suction
    dobotIkconReal(center_redCube2_dropping, jangsvc_, 0 );                         % Pick Cube
    pause(1);
    dobotIkconReal(center_redCube2_moving, jangsvc_, 0 );                           % Go up in the z axis, ( to don't hit any other cube )

    q = dobotIkconReal(final_redCube2_moving, jangsvc_, 0 );                        % Going to final position
    q = dobotIkconReal(final_redCube2_dropping, jangsvc_, 0.5);                     %  Reaching final position 
    
    pause(1);   suctioncupmsg_.EndEffectorState=0; suctioncupsvc_.call(suctioncupmsg_);  pause(1); % Desactivate suction
    dobotIkconReal(final_redCube2_moving, jangsvc_, 0);                             % Go up in the z axis, ( to don't hit any other cube )



% Green cube 1
    dobotIkconReal(center_greenCube1_moving, jangsvc_, 0 );                             % Go To Cube
    suctioncupmsg_.EndEffectorState=1; suctioncupsvc_.call(suctioncupmsg_);             % Activate suction
    dobotIkconReal(center_greenCube1_dropping, jangsvc_, 0);                            % Pick Cube
    pause(1);
    dobotIkconReal(center_greenCube1_moving, jangsvc_, 0);                                 % Go up in the z axis, ( to don't hit any other cube )

    q = dobotIkconReal(final_greenCube1_moving, jangsvc_, 0);                           % Going to final position
    q = dobotIkconReal(final_greenCube1_dropping, jangsvc_, -0.5);                      %  Reaching final position & Adjust rotation

    pause(1);   suctioncupmsg_.EndEffectorState=0; suctioncupsvc_.call(suctioncupmsg_);  pause(1); % Desactivate suction
    dobotIkconReal(final_greenCube1_moving, jangsvc_, 0);                                % Go up in the z axis, ( to don't hit any other cube )



% Green cube 2
    dobotIkconReal(center_greenCube2_moving, jangsvc_, 0);                          % Go To Cube
    suctioncupmsg_.EndEffectorState=1; suctioncupsvc_.call(suctioncupmsg_);         % Activate suction
    dobotIkconReal(center_greenCube2_dropping, jangsvc_, 0);                        % Pick Cube
    pause(1);
    dobotIkconReal(center_greenCube2_moving, jangsvc_, 0);                          % Go up in the z axis, ( to don't hit any other cube )

    q = dobotIkconReal(final_greenCube2_moving, jangsvc_, 0);                       % Going to final position
    q = dobotIkconReal(final_greenCube2_dropping, jangsvc_, -0.5);                  %  Reaching final position & Adjust Rotation of the cube
   
    pause(1);   suctioncupmsg_.EndEffectorState=0; suctioncupsvc_.call(suctioncupmsg_);  pause(1); % Desactivate suction
    dobotIkconReal(final_greenCube2_moving, jangsvc_, 0);                              % Go up in the z axis, ( to don't hit any other cube )


% blue cube 1
    dobotIkconReal(center_blueCube1_moving, jangsvc_, 0);                           % Go To Cube
    suctioncupmsg_.EndEffectorState=1; suctioncupsvc_.call(suctioncupmsg_);         % Activate suction
    dobotIkconReal(center_blueCube1_dropping, jangsvc_, 0);                         % Pick Cube
    pause(1);
    dobotIkconReal(center_blueCube1_moving, jangsvc_, 0);                           % Go up in the z axis, ( to don't hit any other cube )

    q = dobotIkconReal(final_blueCube1_moving, jangsvc_, 0);                       % Going to final position
    q = dobotIkconReal(final_blueCube1_dropping, jangsvc_, -0.5);                  %  Reaching final position  & Adjust Rotation of the cube

    pause(1);   suctioncupmsg_.EndEffectorState=0; suctioncupsvc_.call(suctioncupmsg_);  pause(1); % Desactivate suction
    dobotIkconReal(final_blueCube1_moving, jangsvc_, 0);                           % Go up in the z axis, ( to don't hit any other cube )



% blue cube 2
    dobotIkconReal(center_blueCube2_moving, jangsvc_, 0);                          % Go To Cube
    suctioncupmsg_.EndEffectorState=1; suctioncupsvc_.call(suctioncupmsg_);     % Activate suction
    dobotIkconReal(center_blueCube2_dropping, jangsvc_, 0);                        % Pick Cube
    pause(1);
    dobotIkconReal(center_blueCube2_moving, jangsvc_, 0);                          % Go up in the z axis, ( to don't hit any other cube )

    q = dobotIkconReal(final_blueCube2_moving, jangsvc_, 0);                       % Going to final position
    q = dobotIkconReal(final_blueCube2_dropping, jangsvc_, -0.5);                     %  Reaching final position & Adjust rotation
    
    pause(1);   suctioncupmsg_.EndEffectorState=0; suctioncupsvc_.call(suctioncupmsg_);  pause(1); % Desactivate suction
    dobotIkconReal(final_blueCube2_moving, jangsvc_, 0);                           % Go up in the z axis, ( to don't hit any other cube )




end

