function q = dobotIkconSimulation( position)

% try clear a 
% end
% a = arduino('com5','uno');

x = position(1)   ;
y = position(2) ;
z = position(3) - 0.135 +0.07;


a2 = 0.135;
a3 = 0.1685;

l = sqrt((x^2) + (y^2) );
l = l - 0.062;
D = sqrt((l^2) + (z^2));

t1 = atan(z/l);
t2 = acos( ( (a2^2) + (D^2) - (a3^2))/(2*a2*D));

alpha = t1 + t2;
beta = acos(( (a2^2) + (a3^2) - (D^2))/(2*a2*a3));

q1 = -atan(x/y);
q2 = (pi/2) - alpha;
q3Real = pi - beta - alpha;
q3Model = (pi/2) - q2 +q3Real;
q4 = (pi/2) - (q3Real);

q = [q1 q2 q3Model q4 0];

end