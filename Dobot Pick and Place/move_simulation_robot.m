function move_simulation_robot(q, robot, steps)

s = lspb(0,1,steps);
qMatrix = nan(steps,5);
q_old =  robot.model.getpos();

for i = 1:steps
    qMatrix(i,:) = (1-s(i))*q_old + s(i)*  q;  
end

for i=1:steps   
    robot.model.animate(qMatrix(i,:));      
    drawnow
    %     if (checkButton(a) == 1) break;
    %     end
end

end

